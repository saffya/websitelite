<?php

// Varibales globales de définitions du contenu

// -----------------------------------------------------------//
//                      Personnalize BACKGROUND                     //
// -----------------------------------------------------------//

// Background Header
$mainBgHeader = 'simon-berger-aZjw7xI3QAA-unsplash.jpg';

// Background Page
$pageBgHeader = 'bg-vert.jpg';



// -----------------------------------------------------------//
//                      Personnalize HEAD                     //
// -------------------------------------------------------//

// balise méta / if you have any problem, go to https://www.outils-referencement.com/outils/pages-web/balises-meta for generator

// Site title
$siteTitle = 'WebSite Lite';

// Description (200 characters recommended)

$metaDescription = 'Un mini site de qualité du style CMS simplifié';

// Keywords (max 1000 characters / 7 words recommanded)
$metaKeywords = 'website niwee productions minisite social reseau musique';

// Identifier-Url
$metaIdUrl = 'https://will.niwee.fr/test';

// Favicon
$favicon = 'websitelite.png';


// -----------------------------------------------------------//
//                      Personnalize NavBar                   //
// -----------------------------------------------------------//

$navTitle = 'WebSite Lite';

// file name with extension
$navLogo = 'websitelite.png';

// for ref and accessibility on email
// img folder
$urlImg = '/assets/img/';
$titleImg = '';
$titleImg = $navTitle; //comment if you want your own

$navItem1 = 'About';
$navItem2 = 'Section 2';
$navItem3 = 'Section 3';
$navItem4 = 'Contact';



// -----------------------------------------------------------//
//                      Personnalize EMAIL                    //
// -----------------------------------------------------------//

// From mail
$fromMail = 'contact@test.fr';

// To email
$toMail = 'will@niwee.fr';

// to name
$sendTo = 'Saffya';



// -----------------------------------------------------------//
//                      Personnalize FOOTER                   //
// -----------------------------------------------------------//

// Footer with contacts

$tel = '06 50 75 80 75';
$mail = 'goncalves.saffya@gmail.com';

// Social media : Enter your pseudo/tag
$facebook = 'SasaPapaya';
$twitter = '';
$instagram = 'sasapapaya';

// Author
$metaAuthor = 'NiWee Productions';

// Copyright
$metaCopyright = 'Saffya';

// -----------------------------------------------------------//
//                      Personnalize MAIN-CONTENT             //
// -----------------------------------------------------------//

$mainH1 = 'WebsiteLite OnePage ';

$section_about_h2 = 'Subtitle About';

$section_p = '
<p align="justify"><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk</span><span style="scroll-behavior: smooth; font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px; color: rgb(186, 55, 42);">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk</span><span style="scroll-behavior: smooth; font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px; color: rgb(186, 55, 42);">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">g, remaining essentiall</span></p><p><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;"><br></span></p><p align="justify"><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk</span><span style="scroll-behavior: smooth; font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px; color: rgb(186, 55, 42);">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk</span><span style="scroll-behavior: smooth; font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px; color: rgb(186, 55, 42);">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;">g, remaining essentiall</span><span style="color: rgb(33, 37, 41); font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 1px;"><br></span><br></p>
';

// -----------------------------------------------------------//
//                      Personnalize Section 2-CONTENT            //
// -----------------------------------------------------------//

$page2_h2 = 'Subtitle Section 2';

$page2_img = 'section2.png';

$page2_p = '<p align="justify">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk<span style="color: #ba372a;">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span>g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk<span style="color: #ba372a;">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span>g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>';
$searchString = '"';
$replaceString = "'";
$originalString = "Thi's \"is a programming tutorial\"                    ";

$outputString = str_replace($searchString, $replaceString, $page2_p);

// -----------------------------------------------------------//
//                      Personnalize Section 3-CONTENT            //
// -----------------------------------------------------------//

$page3_h2 = 'Subtitle Section 3';

$page3_img = 'section3.png';

$page3_p = '<p align="justify">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk<span style="color: #ba372a;">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span>g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unk<span style="color: #ba372a;">nown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettin</span>g, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>';
