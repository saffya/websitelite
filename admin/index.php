<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- include libraries(jQuery, bootstrap) -->
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

  <!-- include summernote css/js -->
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
  <title>Document</title>
</head>

<body>
  <form id="main-h1-form" class="rounded p-5 " action="addMainContent.php" method="post">
    <fieldset>
      <div class="row">
        <legend class="text-center mb-5">Informations de la page principale</legend>
        <div class="col-md-6">

          <legend class="text-secondary">Titre principal de la page</legend>
          <div class="form-group ">
          <textarea name='main_h1' id="summernote" placeholder='Titre de la page principal'></textarea>
          </div>
        </div>
      </div>
    </fieldset>
  </form>
  <button id="change" type="submit">Charge TXT</button>
  <script>
    $('#summernote').summernote({
      placeholder: 'Hello stand alone ui',
      tabsize: 2,
      height: 120,
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
      ]
    });

    $("#change").click(function() {
      var val = $.trim($("#summernote").val());
        if (val != "") {

            alert(val.replaceAll('"', "'"));
        }
})
  </script>
</body>

</html>