(function ($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html, body").animate(
          {
            scrollTop: target.offset().top,
          },
          1000,
          "easeInOutExpo"
        );
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $(".js-scroll-trigger").click(function () {
    $(".navbar-collapse").collapse("hide");
  });

  // function to close mobile navbar on click and scroll
  $(document).on("click scroll", function () {
    if ($("#navbarResponsive").hasClass("show")) {
      $(".navbar-collapse").removeClass("show");
      // console.log('yes');
      return;
    }
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $("body").scrollspy({
    target: "#mainNav",
    offset: 100,
  });

  // Collapse Navbar
  var navbarCollapse = function () {
    // if ($("#mainNav").offset().top > 100) {
    //   $("#mainNav").addClass("navbar-shrink");
    // } else {
    //   $("#mainNav").removeClass("navbar-shrink");
    // }
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        $("#mainNav").show(500, 'swing');
        $("#mainNav").addClass("navbar-shrink");
        if ($(window).scrollTop() == 0) {
            $("#mainNav").removeClass("navbar-shrink");
        }
      } else {
        $("#mainNav").hide("slow");
      }
      prevScrollpos = currentScrollPos;
    };
  };

  // test

  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  //   $(window).scroll(navbarCollapse);

  // remove "formulaire de contact" if current page is contact
  if ($("#contact h2").text() == "Contact") {
    // console.log('true');
    $("#formlink").css("display", "none");
  }

  // recaptcha
  function onClick(e) {
    e.preventDefault();
    grecaptcha.ready(function () {
      grecaptcha
        .execute("6Leo5CsaAAAAABd3pbwYHG5Qc4PMAThpfxyI3EYE", {
          action: "submit",
        })
        .then(function (token) {
          // Add your logic to submit to your backend server here.
        });
    });
  }
  $("#submit").click(function () {
    var $captcha = $("#recaptcha"),
      response = grecaptcha.getResponse();

    if (response.length === 0) {
      $(".recap .with-errors").text("La validation du reCAPTCHA est requise");
      if (!$captcha.hasClass("error")) {
        $captcha.addClass("error");
      }
    } else {
      $(".msg-error").text("");
      $captcha.removeClass("error");
      //   alert( 'reCAPTCHA marked' );
    }
  });
})(jQuery); // End of use strict
