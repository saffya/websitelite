<?php require 'variables/global.php'; ?>
<?php header("Content-type: text/css");


$blue                  = '#007bff';
$indigo                = '#6610f2';
$purple                = '#6f42c1';
$pink                  = '#e83e8c';
$red                   = '#dc3545';
$orange                = '#fd7e14';
$yellow                = '#ffc107';
$green                 = '#28a745';
$teal                  = '#64a19d';
$cyan                  = '#17a2b8';
$white                 = '#fff';
$gray                  = '#6c757d';
$graydark              = '#343a40';
$primary               = '#64a19d';
$secondary             = '#6c757d';
$success               = '#28a745';
$info                  = '#17a2b8';
$warning               = '#ffc107';
$danger                = '#dc3545';
$light                 = '#f8f9fa';
$dark                  = '#343a40';
$black                 = '#000000';

// $mainBgHeader = 'bg-sono-nature-min.jpg';
// $pageBgHeader = 'bg-music-min.jpg';








?>

<style>
  #infos p,
  #function p {
    line-height: 2em;
    color: <?php echo $color; ?>;

  }

  .main-masthead {
    position: relative;
    width: 100%;
    min-height: 50vh;
    padding: 10rem 0;
    background: linear-gradient(to top, rgba(0, 0, 0, 0.3) 0%, rgba(0, 0, 0, 0.7) 75%, #000000 100%), url("assets/img/<?php echo $mainBgHeader; ?>");
    /* background-color: <?php echo $pink; ?>; */
    background-position: center;
    background-repeat: no-repeat;
    background-attachment: scroll;
    background-size: cover;
  }

  .main-masthead h1 {
    font-family: 'Montserrat', sans-serif !important;
    margin-top: 150px !important;
    font-size: 3rem;
    line-height: 4rem;
    background: linear-gradient(rgba(255, 255, 255, 1), rgba(255, 255, 255, 1));
    -webkit-background-clip: text;
    background-clip: text;
    color: <?php echo $white ?>;
  }

  @media (max-width: 650px) {
    .main-masthead h1 {
      margin-top: 20px !important;
      font-size: 1.5rem;
    }

    .main-masthead {
      height: 40vh !important;
    }
  }
</style>