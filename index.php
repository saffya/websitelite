<?php require "templateParts/head.php"; ?>


<!-- Masthead-->
<?php require "templateParts/header.php"; ?>
<!-- About-->
<section class="about-section " id="about">
    <div class="container col-10">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class=" mb-5 text-center"><?php echo $section_about_h2 ?></h2>
                <p><?php echo $section_p ?></p>
            </div>
        </div>
    </div>
</section>

<div class="border-img">

</div>

<!-- Section 2-->
<section class="function-section section-img" id="section2">
    <div class="container col-12">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class="text-center"><?php echo $page2_h2 ?></h2>
                <img class="mb-4" src="assets/img/<?php echo $page2_img ?>" alt="" />
                <div class="d-flex">

                    <p class="col-1">
                        <?php echo $outputString ?>

                    </p>
                </div>

            </div>
        </div>

    </div>
</section>

<!-- section 3-->
<section class="infos-section section-img" id="section3">
    <div class="container col-12">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2 class="my-5 text-center"><?php echo $page3_h2 ?></h2>
                <img class=" mb-4" src="assets/img/<?php echo $page3_img ?>" alt="" />
                <div class="d-flex">
                    <p class="mr-5">
                        <?php echo $page3_p ?>
                    </p>

                </div>

            </div>
        </div>

    </div>
</section>

<section id='contact'>
    <div class="container my-5">

        <h2 class="text-center my-5">Contact</h2>
        <div class="row">

            <div class="col-12 col-lg-offset-2">

                <form id="contact-form" method="post" action="contact-2.php" role="form">
                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Prénom *</label>
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Votre prénom *" required="required" data-error="Indiquez votre prénom.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_lastname">Nom *</label>
                                    <input id="form_lastname" type="text" name="surname" class="form-control" placeholder="Votre nom *" required="required" data-error="Indiquez votre nom.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Email *</label>
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Entrer votre email *" required="required" data-error="Indiquez votre email.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">Téléphone</label>
                                    <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Entrer un téléphone">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Message *</label>
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Message... *" rows="4" required="required" data-error="Laissez nous un message."></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <!-- <div class="col-md-12 recap">
                                    <div class="g-recaptcha my-3 " id="recaptcha" data-sitekey="6Lfa6isaAAAAAJwL_ynMu8P41dMcWY_ldLOKM0Am"></div>
                                    <div class="help-block with-errors"></div>
                                </div> -->

                            <div class="col-md-12 mt-2">
                                <input type="submit" id='submit' class="btn btn-success btn-send" value="ENVOYER">
                            </div>
                        </div>
                        <div class="messages"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-muted"><strong>*</strong> Champs obligatoires .</p>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
            <!-- <div class="col-12 col-lg-offset-2">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d6597.213730529225!2d3.474393836969673!3d45.9555448091753!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1618498040410!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div> -->
        </div>

    </div>


</section>
<?php require_once "templateParts/footer.php"; ?>