<!-- Navigation-->
<nav class="navbar navbar-expand-xl fixed-top" id="mainNav">
    <div class="container">

        <a class="navbar-brand js-scroll-trigger neuro-title" href="index.php"><img id='nav-logo' title="<?php echo $navTitle ?>" alt="Logo de <?php echo $navTitle ?>" src="assets/img/<?php echo $navLogo ?>" alt=""></a>
        <a class="navbar-brand js-scroll-trigger neuro-title" href="index.php"><?php echo $navTitle ?></a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>


        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item "><a class="nav-link js-scroll-trigger" href="#about"><?php echo $navItem1 ?></a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#section2"
                <?php
                echo "#section1";


                ?>
                
                
                ">
                        <?php echo $navItem2 ?>
                        <!-- <?php
                                if (!empty($navItem2)) {

                                    $oldName = "Frank" . '.php';
                                    $newName = $navItem2 . ".php";

                                    if (file_exists($oldName)) {
                                        $renamed = rename($oldName, $newName);

                                        if (!$renamed) {
                                            echo "The file has not been successfully renamed";
                                        } else {
                                            echo "The file has been successfully renamed";
                                        }
                                    } else {
                                        echo ('le fichier n\'existe pas');
                                    }
                                } else {
                                    echo 'page2';
                                }


                                ?> -->
                    </a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#section3"><?php echo $navItem3 ?></a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact"><?php echo $navItem4 ?></a></li>
            </ul>
        </div>

    </div>
</nav>
