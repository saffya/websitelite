<!DOCTYPE html>
<html lang="fr">

<?php include "variables/global.php"; ?>

<head>
    <title><?php echo $siteTitle ?></title>
    <meta name="Content-Type" content="UTF-8">
    <meta name="Content-Language" content="fr">
    <meta name="Description" content="<?php echo $metaDescription ?>">
    <meta name="Keywords" content="<?php echo $metaKeywords ?>">
    <meta name="Copyright" content="<?php echo $metaCopyright ?>">
    <meta name="Identifier-Url" content="<?php echo $metaIdUrl ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="author" content="<?php echo $metaAuthor ?>" />
    <link rel="icon" type="image/x-icon" href="assets/img/<?php echo $favicon ?>" />

    

    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />

    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="assets/css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="screen" href="css.php">


</head>


<body id="page-top">

    <?php require "templateParts/nav.php"; ?>