 <!-- Contact-->
 <section class="contact-section bg-black">
     <div class="container">
         <div class="row color-link">
             <div class="col-md-6 mb-3 mb-md-0">
                 <div class="card py-4 h-100">
                     <div class="card-body text-center">
                         <i class="fas fa-envelope text-primary mb-2"></i>
                         <p class="text-uppercase m-0">Email</p>
                         <hr class="my-4" />
                         <div class="small text-black-50"><a title="Envoyer un email ?" href="mailto:<?php echo $mail ?>"><?php echo $mail ?></a></div>
                         <br>
                         <div id='formlink' class="small text-black-50"><a href="contact.php">Formulaire de contact</a></div>
                     </div>
                 </div>
             </div>
             <div class="col-md-6 mb-3 mb-md-0">
                 <div class="card py-4 h-100">
                     <div class="card-body text-center">
                         <i class="fas fa-mobile-alt text-primary mb-2"></i>
                         <p class="text-uppercase m-0">Contact</p>
                         <hr class="my-4" />
                         <div class="small text-black-50"><a title="Appeler ce numéro ?" href="tel:+33<?php echo $tel ?>"><?php echo $tel ?></a></div>

                     </div>
                 </div>
             </div>
         </div>
         <div class="social d-flex justify-content-center">
             <a class="mx-2" title="Lien vers le compte facebook " target="_blanck" href="https://www.facebook.com/<?php echo $facebook ?>"><i class="fab fa-facebook-f"></i></a>
             <a class="mx-2" title="Lien vers le compte twitter " target="_blanck" href="https://www.twitter.com/<?php echo $twitter ?>"><i class="fab fa-twitter"></i></a>
             <a class="mx-2" title="Lien vers le compte instagram " target="_blanck" href="https://www.instagram.com/<?php echo $instagram ?>"><i class="fab fa-instagram"></i></a>
         </div>
     </div>
 </section>
 <!-- Footer-->
 <footer class="footer bg-black small text-center text-white-50">
     <div class="container">Copyright © <?php echo $metaCopyright ?> by <a target='_blanck' title="Site de l'agence web NiWee Productions" href="https://niwee.fr"><?php echo $metaAuthor ?></a> <?php echo date('Y'); ?></div>
 </footer>


 <!-- Footer part who contain all script -->
 <!-- Bootstrap core JS-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
 <!-- Third party plugin JS-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
 <!-- Core theme JS-->
 <script src="assets/js/scripts.js"></script>

 <!-- Form validator -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
 <script src="assets/js/contact-2.js"></script>

 <!-- Font Awesome icons (free version)-->
 <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>

 </body>

 </html>