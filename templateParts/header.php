<header class="main-masthead">
    <div class="container d-flex align-items-center">
        <div class="mx-auto text-center">
            <h1 class="mx-auto my-0 text-uppercase"><?php echo $mainH1 ?></h1>
        </div>
    </div>
</header>